[![pipeline status](https://gitlab.com/ekkebus/scrum-events/badges/master/pipeline.svg)](https://gitlab.com/ekkebus/scrum-events/-/commits/master)
[![coverage report](https://gitlab.com/ekkebus/scrum-events/badges/master/coverage.svg)](https://gitlab.com/ekkebus/scrum-events/-/commits/master)
---

## Play now

👉 [`https://ekkebus.gitlab.io/scrum-events/`](https://ekkebus.gitlab.io/scrum-events/) 👈


---

Initial project based on 'Scrum Events and Activities' card game which is based on The Scrum™ Guide (2017) and is released under Creative Commons Attribution-ShareAlike 4.0 International (CC BY SA 4.0) https://creativecommons.org/licenses/by-sa/4.0/

During the development of the project the following other content has been added:

**Scrum Roles** card game. Based on The Scrum™ Guide (2017) and is released under Creative Commons Attribution-ShareAlike 4.0 International (CC BY SA 4.0) https://creativecommons.org/licenses/by-sa/4.0/

**Scrum values** By Julio Oliveira. Released under Creative Commons Attribution-ShareAlike 3.0 (CC BY-SA 3.0) https://creativecommons.org/licenses/by-sa/3.0/

**Myth or Fact** Copyright 2020 by Ziryan Consulting (http://www.myth-or-fact.nl/). Released under Attribution-NonCommercial-ShareAlike 4.0 (CC BY-NC-SA 4.0) https://creativecommons.org/licenses/by-nc-sa/4.0/

**Scrum mythbusters** Copyright 2019 by The Liberators (Christiaan Verwijs and Barry Overeem), https://medium.com/the-liberators/scrum-mythbusters/home

**Values of The Agile Manifesto** By Swen-Peter Ekkebus released under Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) https://creativecommons.org/licenses/by-sa/4.0/. Based on Manifesto for Agile Software Development by Ken Schwaber, Jeff Sutherland, et al. © 2001.

**Product Management activities** Copyright 2020 Kai Stevens https://theproductowner.coach. Released under Attribution-NonCommercial-ShareAlike 4.0 (CC BY-NC-SA 4.0) https://creativecommons.org/licenses/by-nc-sa/4.0/
