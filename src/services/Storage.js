import EventService from "./EventService";
import {HubManager} from "./HubService";

export const API = {
    createGame(name, templateId, directFeedback) {
        Storage.showLoadingScreen = true;
        return EventService.postCreate({
            name,
            gameTemplateId: templateId,
            directFeedback
        }, Storage.userBearer).then(response => {
            Storage.game = response.data;
            Storage.totalCards = response.data.cardStack.length;
            localStorage.setItem("CurrentGameJoinCode", Storage.game.joinCode);
            Storage.tutorialCounter = 1;
            Storage.timer.currentTime = Storage.game.timerDuration * 60;
            Storage.stopFireworks = true;
            if(Methods.isMobile()){    
                Methods.stopTutorial()
                Storage.tutorialCounter = 0
                Storage.isMobile = true
            }

            Methods.closeModal();
        }).catch(error => {
            Storage.openModal = {
                modalType: "APIError",
                modalCode: error.response.status,
                modalText: error.response.data.message + " - Creating game",
                originalModalType: "startGame"
            }
        }).finally(() => {
            Storage.showLoadingScreen = false;
        });
    },

    setSettings(settings, initialSettings = {}) {
        if (JSON.stringify(settings) === JSON.stringify(initialSettings)) {
            if ('isHidden' in Storage.game && Storage.game.isHidden === true) {
                Storage.openModal = {
                    modalType: ('closeToType' in Storage.openModal ? Storage.openModal.closeToType : "startGame")
                }
            } else {
                Methods.closeModal();
            }
            return;
        }
        Storage.showLoadingScreen = true;
        EventService.settings(settings, Storage.userBearer).then(response => {
            Storage.userData = response.data;
            if ('isHidden' in Storage.game && Storage.game.isHidden === true) {
                Storage.openModal = {
                    modalType: ('closeToType' in Storage.openModal ? Storage.openModal.closeToType : "startGame")
                }
            } else {
                Methods.closeModal();
            }
        }).catch(error => {
            this._handleErrors(error.response.data);
        }).finally(() => {
            Storage.showLoadingScreen = false;
        });
    },

    putCard(cardId, columnId, fromColumnId = null) {
        if (Storage.game.isFinished) {
            this._handleErrors({message: "The game has already finished."});
            return;
        }
        if(Storage.game.directFeedback && (fromColumnId !== null) && (Storage.wrongCards.includes(cardId) || Storage.correctCards.includes(cardId))   ){
            this._handleErrors({message: "A placed card cannot be moved"});
            return;
        }
        if (!Storage.game.isTutorialFinished) {
            this._handleErrors({message: "Please finish the tutorial first."});
            return;
        }
        //Storage.showLoadingScreen = true;
        EventService.putCard({
            cardId, columnId, fromColumnId
        }, Storage.userBearer).then(response => {
            if (Storage.game.directFeedback){
                Storage.game = response.data.game;
                Methods.checkCards(response.data.wrongCards, response.data.correctCards);
            } else {
                Storage.game = response.data;
            }
        }).catch(error => {
            this._handleErrors(error.response.data);
        });
    },

    finishGame() {
        Storage.showLoadingScreen = true;
        return EventService.finish(
            Storage.userBearer
        ).then(response => {
            Storage.game = response.data.game;
            Methods.checkCards(response.data.wrongCards, response.data.correctCards);
            Methods.setGameState();
            if (Storage.wrongCards.length === 0) Storage.stopFireworks = false
            Storage.tutorialCounter = 6;
        }).catch(error => {
            this._handleErrors(error.response.data);
        }).finally(() => {
            Storage.showLoadingScreen = false;
        });
    },

    makeNote(note, cardId) {
        if (!Storage.game.isTutorialFinished) {
            this._handleErrors({message: "Please finish the tutorial first."});
            return;
        }
        Storage.showLoadingScreen = true;
        EventService.addNote({
            note, cardId
        }, Storage.userBearer).then(response => {
            Storage.game = response.data;
        }).catch(errors => {
            this._handleErrors(errors.response.data);
        }).finally(() => {
            Storage.showLoadingScreen = false;
        });
    },

    resetGame() {
        if (!Storage.game.isTutorialFinished) {
            this._handleErrors({message: "Please finish the tutorial first."});
            return;
        }
        Storage.showLoadingScreen = true;
        EventService.resetGame(
            Storage.userBearer
        ).then(response => {
            Storage.game = response.data;
        }).catch(errors => {
            this._handleErrors(errors.response.data);
        }).finally(() => {
            Storage.showLoadingScreen = false;
        });
    },

    showTimePassed() {
        Storage.showLoadingScreen = true;
        EventService.showTimePassed(
            Storage.userBearer
        ).then(response => {
            Storage.game = response.data;
        }).catch(errors => {
            this._handleErrors(errors.response.data);
        }).finally(() => {
            Storage.showLoadingScreen = false;
        });
    },

    timerAddMinutes(minutes) {
        if (Storage.game.isFinished) {
            this._handleErrors({message: "The game is already finished."});
            return;
        }
        if (!Storage.game.isTutorialFinished) {
            this._handleErrors({message: "Complete the tutorial before adding more time."});
            return;
        }
        let seconds = minutes * 60;
        if ((seconds + Storage.timer.currentTime) > 3600) {
            this._handleErrors({message: "You can't add more time."});
            return;
        }

        Storage.showLoadingScreen = true;
        EventService.timerAddMinutes({
                minutes
            }, Storage.userBearer
        ).then(response => {
            Storage.game.timerDuration = response.data.timerDuration;
        }).catch(errors => {
            this._handleErrors(errors.response.data);
        }).finally(() => {
            Storage.showLoadingScreen = false;
        });
    },

    _handleErrors(errorData, errorResponse = null, errorModalText = "") {
        if (errorResponse !== null && ((errorResponse.status > 300 && errorResponse.status < 400) || (errorResponse.status > 400 && errorResponse.status < 600))) {
            Storage.openModal = {
                modalType: "APIError",
                modalCode: errorResponse.status,
                modalText: errorResponse.data.message + errorModalText,
                originalModalType: "modalType" in Storage.openModal ? Storage.openModal.modalType : ""
            }
        }
        let randomId = '_' + Math.random().toString(36).substr(2, 9);
        if ("message" in errorData) {
            Storage.snackbars = {...Storage.snackbars, [randomId]: {text: errorData.message, type: "error"}}
        } else if ("errors" in errorData) {
            Object.keys(errorData.errors).forEach((item) => {
                if (Array.isArray(errorData.errors[item])) {
                    errorData.errors[item].forEach(v => {
                        Storage.snackbars = {...Storage.snackbars, [randomId]: {text: v, type: "error"}}
                    });
                } else {
                    Storage.snackbars = {...Storage.snackbars, [randomId]: {text: errorData.errors[item], type: "error"}}
                }
            });
        } else {
            Storage.snackbars = {...Storage.snackbars, [randomId]: {text: "Unknown error occurred.", type: "error"}}
        }
    },

    skipCard() {
        if (!Storage.game.isTutorialFinished) {
            this._handleErrors({message: "Please finish the tutorial first."});
            return;
        }
        return EventService.skipCard(Storage.userBearer).then(response => {
            Storage.game = response.data;
        }).catch(errors => {
            this._handleErrors(errors.response.data);
        });
    },
    getGames() {
        Storage.showLoadingScreen = true;
        return EventService.getGames(Storage.userBearer).then(response => {
            Storage.gameList = response.data;
        }).catch(errors => {
            Storage.openModal = {
                modalType: "APIError",
                modalCode: errors.response.status,
                modalText: errors.response.data.message + " - Getting Games",
                originalModalType: "startGame"
            }
        }).finally(() => {
            Storage.showLoadingScreen = false;
        });
    },
    registerUser(userName) {
        Storage.showLoadingScreen = true;
        EventService.registerUser({
            username: userName
        }).then(response => {
            let token = response.data.token;
            Storage.userBearer = token;
            localStorage.setItem("accessToken", token);
            Storage.userData = response.data.user;
            HubManager.initConnection(Storage.userBearer).then(_ => {});
            this.getGames().then(() => {
                Storage.openModal = {
                    modalType: 'selectedGame' in window.vm.$route.query ? "startGame" : "joinGame"
                }
            });
        }).catch(errors => {
            this._handleErrors(errors.response.data, errors.response, " - Register user");
        }).finally(() => {
            Storage.showLoadingScreen = false;
        });
    },
    isTokenValid() {
        if (localStorage.getItem("accessToken") !== null) {
            Storage.userBearer = localStorage.getItem("accessToken");
            Storage.showLoadingScreen = true;
            EventService.isTokenValid(Storage.userBearer).then(response => {
                this.getGames().then(() => {
                    if (response.data.game !== null) {
                        Storage.game = response.data.game;
                        Storage.totalCards = Methods.countAllCards();
                        if (!Storage.game.isTutorialFinished) {
                            Storage.timer.currentTime = Storage.game.timerDuration * 60;
                        }

                        Storage.tutorialCounter = (Storage.game.isTutorialFinished ? 0 : 1);
                        if (Storage.game.directFeedback){
                            if (response.data.correctCards !== null && response.data.wrongCards !== null) {
                                Methods.checkCards(response.data.wrongCards, response.data.correctCards);
                            }
                        }
                        if (Storage.game.isFinished) {
                            if (response.data.correctCards !== null && response.data.wrongCards !== null) {
                                Methods.checkCards(response.data.wrongCards, response.data.correctCards);
                                Methods.setGameState();
                                Storage.totalCards = response.data.wrongCards.length + response.data.correctCards.length;
                            }
                        }
                    } else {
                        if (localStorage.getItem("CurrentGameJoinCode") !== null) {
                            let joinCode = localStorage.getItem("CurrentGameJoinCode");
                            this.joinGame(joinCode);
                        } else {
                            Storage.openModal = {
                                modalType: 'selectedGame' in window.vm.$route.query ? "startGame" : "joinGame"
                            }
                        }
                    }

                    HubManager.initConnection(Storage.userBearer).then(_ => {});
                });

                Storage.userData = response.data.user;
            }).catch(_ => {
                Storage.userBearer = "";
                localStorage.removeItem("accessToken");
                Storage.openModal = {
                    modalType: "register"
                }
            }).finally(() => {
                Storage.showLoadingScreen = false;
            });
        } else {
            Storage.openModal = {
                modalType: "register"
            }
        }
    },

    leaveGame() {
        Storage.showLoadingScreen = true;
        return EventService.leaveGame(Storage.userBearer).then(_ => {
            Storage.game.isHidden = true;
            localStorage.removeItem("CurrentGameJoinCode");
        }).catch(errors => {
            this._handleErrors(errors.response.data);
        }).finally(() => {
            Storage.showLoadingScreen = false;
        });
    },

    finishTutorial() {
        if (Storage.game.isFinished) {
            return;
        }
        if (Storage.game.isTutorialFinished) {
            return;
        }

        Storage.showLoadingScreen = true;
        EventService.finishTutorial(Storage.userBearer).then(response => {
            Storage.game = response.data;
        }).catch(errors => {
            this._handleErrors(errors.response.data);
        }).finally(() => {
            Storage.showLoadingScreen = false;
        });
    },

    joinGame(gameToken) {
        Storage.showLoadingScreen = true;
        return EventService.joinGame({
            gameToken: gameToken
        }, Storage.userBearer).then(response => {
            Storage.game = response.data.game;
            Storage.totalCards = Methods.countAllCards();
            Storage.stopFireworks = true;
            localStorage.setItem("CurrentGameJoinCode", Storage.game.joinCode);

            if (!Storage.game.isTutorialFinished) {
                Storage.timer.currentTime = Storage.game.timerDuration * 60;
            }

            Storage.tutorialCounter = (Storage.game.isTutorialFinished ? 0 : 1);

            if (Storage.game.directFeedback) {
                if (response.data.correctCards !== null && response.data.wrongCards !== null) {
                    Methods.checkCards(response.data.wrongCards, response.data.correctCards);
                }
            }
            if (Storage.game.isFinished) {
                if (response.data.correctCards !== null && response.data.wrongCards !== null) {
                    Methods.checkCards(response.data.wrongCards, response.data.correctCards);
                    Storage.totalCards = response.data.wrongCards.length + response.data.correctCards.length;
                }
            }

            Methods.closeModal();
        }).catch(errors => {
            localStorage.removeItem("CurrentGameJoinCode");
            this._handleErrors(errors.response.data);
            Storage.openModal = {
                modalType: "joinGame"
            }
        }).finally(_ => {
            Storage.showLoadingScreen = false;
        });
    },
    showEventList(){
       return EventService.showEvents(
            Storage.userBearer
        ).then(response => {
            Storage.eventList = response.data;
        }).catch(errors => {
            this._handleErrors(errors.response);
        });
    },

    showStartTimer(){
        return EventService.showTimer(
            Storage.userBearer
        ).then(response => {
            Storage.startTimer = response.data;
        }).catch(errors => {
            this._handleErrors(errors.response);
        });
    },
    showTotalGames(){
        return EventService.showTotalGames(
            Storage.userBearer
        ).then(response => {
            Storage.totalGamesList = response.data;
        }).catch(errors => {
            this._handleErrors(errors.response);
        });
    },
    logout() {
        EventService.logout(Storage.userBearer).then(_ => {
            Storage.userBearer = "";
            localStorage.removeItem("CurrentGameJoinCode");
            localStorage.removeItem("accessToken");
            Storage.openModal = {
                modalType: "register"
            }
        }).catch(errors => {
            this._handleErrors(errors.response.data, errors.response, " - Logout user");
        });
    }
};

export const Methods = {
    checkCards(wrongCards, correctCards) {
        Storage.correctCards = [];
        Storage.wrongCards = [];
        for (let i = 0; i < correctCards.length; i++) {
            Storage.correctCards.push(correctCards[i].id);
        }
        for (let j = 0; j < wrongCards.length; j++) {
            Storage.wrongCards.push(wrongCards[j].id);
        }
    },

    isMobile() {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    },

    setGameState() {
        if (Storage.correctCards.length > Storage.wrongCards.length) {
            //> 50% correct
            Storage.endGameState = 2
        } else {
            //< 50% correct
            Storage.endGameState = 1
        }
    },

    countAllCards() {
        let count = 0;
        count += Storage.game.cardStack.length;
        Storage.game.columns.forEach(column => {
            count += column.cards.length;
        });

        return count;
    },

    setGameTimer() {
        setInterval(() => {
            if ('isHidden' in Storage.game && Storage.game.isHidden) {
                return;
            }

            this.refreshTime();
        }, 500);
    },

    refreshTime() {
        if (('isHidden' in Storage.game && Storage.game.isHidden) || !Storage.game.isTutorialFinished) {
            return;
        }

        const startDateTime = Date.parse(Storage.game.startTime + (Storage.game.startTime[Storage.game.startTime.length -1] !== "Z" ? "Z" : ""));
        const delta = Date.now() - startDateTime;

        if (Storage.game.isFinished) {
            const endDateTime = Date.parse(Storage.game.finishTime + (Storage.game.finishTime[Storage.game.finishTime.length -1] !== "Z" ? "Z" : ""));
            Storage.timer.currentTime = (Storage.game.timerDuration * 60) - Math.floor(Math.abs(endDateTime - startDateTime) / 1000);
        } else {
            Storage.timer.currentTime = (Storage.game.timerDuration * 60) - Math.floor(Math.abs(delta) / 1000);
        }
    },

    showSnackbar(message, type) {
        let randomId = '_' + Math.random().toString(36).substr(2, 9);
        Storage.snackbars = {...Storage.snackbars, [randomId]: {text: message, type: type}}
    },

    isModalOpen(modalType) {
        return Storage.openModal !== null && Object.keys(Storage.openModal).length !== 0 && Storage.openModal.modalType === modalType;
    },

    closeModal() {
        Storage.openModal = {};
    },
    increaseTutorialCounter() {
        Storage.tutorialCounter++;
    },
    decreaseTutorialCounter() {
        Storage.tutorialCounter--;
    },
    stopTutorial() {
        Storage.tutorialCounter = 0;
        API.finishTutorial();
    }
};

export const Storage = {
    game: {
        isHidden: true,
        id: 0,
        name: "GameName",
        isFinished: false,
        directFeedback: false,
        timerDuration: 0,
        copyrightString: "",
        currentTurnPlayerId: null,
        columns: [
            {
                id: 0,
                title: "Column title",
                cards: [
                    {
                        id: 0,
                        description: "Desccc",
                        column: null,
                        explanation: "Blabla",
                        note: null,
                        correctColumnId: 0
                    }
                ]
            }
        ],
        cardStack: [
            {
                id: 0,
                description: "Desccc",
                column: null,
                explanation: "Blabla",
                note: null,
                correctColumnId: 12
            },
            {
                id: 1,
                description: "Desccc",
                column: null,
                explanation: "Blabla",
                note: null,
                correctColumnId: 12
            }
        ],
        finishTime: null,
        isTutorialFinished: false
    },
    userData: {
        darkMode: true
    },
    userBearer: "",
    snackbars: {},
    correctCards: [],
    wrongCards: [],
    openModal: {
        modalType: "",
        modalCode: 0,
        modalText: "",
        originalModalType: ""
    },
    totalCards: 0,
    timer: {
        currentTime: 0,
        totalTime: 600
    },
    startTimer:"",
    eventList:[],
    gameList:[],
    totalGamesList:0,
    showSideBar: false,
    showLoadingScreen: false,
    tutorialCounter: 0,
    endGameState: 0,
    stopFireworks : true,
    isMobile: false,
};
