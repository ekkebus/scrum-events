import * as SignalR from "@microsoft/signalr";
import {API, Methods, Storage} from "./Storage";

export let _connection;

export const HubManager = {
    async initConnection(bearer) {
        _connection = new SignalR.HubConnectionBuilder()
            .withUrl(process.env.VUE_APP_API_URL
                + "/game/live-hub", {
                accessTokenFactory: () => bearer
            })
            .configureLogging(SignalR.LogLevel.Error)
            .withAutomaticReconnect()
            .build();

        _connection.onreconnecting(() => Storage.showLoadingScreen = true);
        _connection.onreconnected(() => {
            Storage.showLoadingScreen = false;
            if (localStorage.getItem("CurrentGameJoinCode") !== null) {
                let joinCode = localStorage.getItem("CurrentGameJoinCode");
                API.joinGame(joinCode);
            }
        });

        _connection.on("TestMethod", (data) => {
            data = JSON.parse(data);
            console.log(data);
        });

        _connection.on("GameUpdate", (data) => {
            data = JSON.parse(data);
            let isCurrentlyFinished = Storage.game.isFinished;
            Storage.game = data.game;

            if (!Storage.game.isTutorialFinished) {
                Storage.timer.currentTime = Storage.game.timerDuration * 60;
            }

            if (Storage.game.directFeedback) {
                if (data.correctCards !== null && data.wrongCards !== null) {
                    Methods.checkCards(data.wrongCards, data.correctCards);
                }
            }
            if (Storage.game.isFinished) {
                if (data.correctCards !== null && data.wrongCards !== null) {
                    Methods.checkCards(data.wrongCards, data.correctCards);
                    Storage.totalCards = data.wrongCards.length + data.correctCards.length;
                    if (Storage.wrongCards.length === 0) Storage.stopFireworks = false
                }
                Methods.setGameState();
            }

            if (isCurrentlyFinished !== data.game.isFinished) {
                if (Storage.endGameState === 2) {
                    window.vm.$confetti.start({
                        defaultColors: [
                            '#8854d0', '#fa8231'
                        ]
                    });
                    setTimeout(() => window.vm.$confetti.stop(),2500);
                }
            }
        });

        await _connection.start()
            .catch(e => {console.log("SignalR Hub Error: " + e);});
    }
};
