import axios from "axios";

const apiClient = axios.create({
  baseURL: process.env.VUE_APP_API_URL + "/game",
  withCredentials: false, // This is the default
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  },
  timeout: 10000
});

const apiUserClient = axios.create({
  baseURL: process.env.VUE_APP_API_URL + "/user",
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  },
  timeout: 10000
});

export default {
  postCreate(gameModel, bearer) {
    return apiClient.post("/create", gameModel, {
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  finish(bearer) {
    return apiClient.post("/finish", null, {
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  skipCard(bearer) {
    return apiClient.post("/card/skip", null,{
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  addNote(noteModel, bearer) {
    return apiClient.post("/card/note/add", noteModel, {
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  putCard(putCardModel, bearer) {
    return apiClient.post("/card/put", putCardModel, {
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  resetGame(bearer){
    return apiClient.post("/reset", null, {
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  getGames(bearer){
    return apiClient.get("/games/list", {
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  showTimePassed(bearer){
    return apiClient.get("/timer/time-passed", {
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    })
  },
  timerAddMinutes(minutes, bearer){
    return apiClient.post("/timer/add-minutes", minutes, {
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  registerUser(usernameModel) {
    return apiUserClient.post("/register", usernameModel);
  },
  isTokenValid(bearer) {
    return apiUserClient.get("/verify", {
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  leaveGame(bearer) {
    return apiClient.post("/leave", null, {
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  finishTutorial(bearer) {
    return apiClient.post("/tutorial/finish", null,{
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  showEvents(bearer) {
    return apiClient.get("/events/all",{
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  showTimer(bearer) {
    return apiClient.get("/events/create-time",{
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  showTotalGames(bearer) {
    return apiClient.get("/events/total-games",{
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  joinGame(joinGameModel, bearer) {
    return apiClient.post("/join", joinGameModel, {
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  settings(settingsModel, bearer) {
    return apiUserClient.post("/settings", settingsModel, {
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    });
  },
  logout(bearer) {
    return apiUserClient.post("/logout", null, {
      headers: {
        Authorization: `Bearer ${bearer}`
      }
    })
  }
};
