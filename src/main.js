import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import App from "./App";
import router from "./router";
import { API, Methods, Storage } from "./services/Storage";
import { Utils } from "./utils/Utils";
import { HubManager } from "./services/HubService";
import Argon from "./plugins/argon-kit";
import * as Sentry from '@sentry/browser';
import { Vue as VueIntegration } from '@sentry/integrations';
import VueConfetti from 'vue-confetti';
import VueClipboard from "vue-clipboard2";
import * as firebase from "firebase/app";
import "firebase/analytics";

// Firebase configuration
let firebaseConfig = {
  apiKey: "AIzaSyDGYjdpqFCYqslQKsriNV7rL2xR34nrZrw",
  authDomain: "ekkebus-gitlab-io-scrum-events.firebaseapp.com",
  databaseURL: "https://ekkebus-gitlab-io-scrum-events.firebaseio.com",
  projectId: "ekkebus-gitlab-io-scrum-events",
  storageBucket: "ekkebus-gitlab-io-scrum-events.appspot.com",
  messagingSenderId: "250318328859",
  appId: "1:250318328859:web:00dc46b6b3bda8579a94be",
  measurementId: "G-X9BTCP8E3H"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

if (process.env.NODE_ENV === 'production') {
  Sentry.init({
    dsn: 'https://90f2cd4004a84baf8ad347211b792f83@o393255.ingest.sentry.io/5242081',
    integrations: [new VueIntegration({ Vue, attachProps: true })],
  });
}

Vue.config.productionTip = false;
Vue.use(Argon);
Vue.use(VueConfetti);
Vue.use(VueClipboard);

let vm = new Vue({
  router,
  data: {
    HubManager: HubManager,
    APIStorage: Storage,
    APICaller: API,
    APIMethods: Methods,
    Utils: Utils,
    Firebase: firebase.analytics()
  },
  render: h => h(App)
}).$mount("#app");

window.vm = vm;
