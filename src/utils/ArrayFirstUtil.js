/**
 * Used in ./src/components/scrum/NavBar.vue
 */
if (!Array.prototype.first){
    Array.prototype.first = function(){
        return this[0] !== undefined ? this[0] : null;
    };
}
