import {Storage} from "../services/Storage";

/**
 * Returns the selected 'deck of cards' / game from the storage
 */
export const Utils = {
    getCardById(id) {
        for (let card of Storage.game.cardStack) {
            if (card.id === id) return card;
        }

        for (let column of Storage.game.columns) {
            for (let card of column.cards) {
                if (card.id === id) return card;
            }
        }
    }
};
