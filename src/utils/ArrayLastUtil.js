if (!Array.prototype.last) {
    Array.prototype.last = function () {
        return this[this.length - 1] !== undefined ? this[this.length - 1] : null;
    };
}