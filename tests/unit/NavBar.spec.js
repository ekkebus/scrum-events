import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import { Storage } from "../../src/services/Storage";
import NavBar from "../../src/components/scrum/NavBar";

describe('components/scrum/NavBar - sanity check', () => {
    let wrapper = null

    beforeEach(() => {
        wrapper = mountWithPlugins(NavBar);
    });

    it('Test Wrapper works', () => {
        expect(wrapper.exists()).toBe(true)
    });
});

describe('components/scrum/NavBar - methods', () => {
    //Storage.correctCards = [{id:1}, {id:2}]
    //Storage.wrongCards = [{id:3}, {id:4}]
    //Storage.totalCards = 4
    
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(NavBar);
    });

    it('Show restart modal', () => {
        wrapper.vm.showRestartModal();
        expect(Storage.openModal.modalType).toBe("restartGame");
    });

    it('Show reset modal', () => {
        wrapper.vm.showResetModal();
        expect(Storage.openModal.modalType).toBe("resetGame");
    });

    it('open/close sidebar', () => {
        let sidebar = Storage.showSideBar;
        wrapper.vm.openCloseSidebar();
        expect(Storage.showSideBar).toBe(!sidebar);
        wrapper.vm.openCloseSidebar();
        expect(Storage.showSideBar).toBe(sidebar);
    });

    it('Restart tutorial when game is not finished', () => {
        Storage.game.isFinished = false;
        wrapper.vm.restartTutorial();
        expect(Storage.tutorialCounter).toBe(1);
    });

    it('Open settings modal', () => {
        wrapper.vm.settings();
        expect(Storage.openModal.modalType).toBe("optionsModal");
    });

    it('Stop fireworks', () => {
        Storage.stopFireworks = false;
        wrapper.vm.stopFirework();
        expect(Storage.stopFireworks).toBe(true);
    });

    it('Generate random color, within range', () => {
        expect(wrapper.vm.randomColor(0)).toEqual('red');
        expect(wrapper.vm.randomColor(1)).toEqual('purple');
        expect(wrapper.vm.randomColor(2)).toEqual('green');
        expect(wrapper.vm.randomColor(3)).toEqual('blue');
    });

    it('Generate random color, above range', () => {
        expect(wrapper.vm.randomColor(4)).toEqual('red');
        expect(wrapper.vm.randomColor(8)).toEqual('red');
        expect(wrapper.vm.randomColor(5)).toEqual('purple');
        expect(wrapper.vm.randomColor(9)).toEqual('purple');
        expect(wrapper.vm.randomColor(6)).toEqual('green');
        expect(wrapper.vm.randomColor(10)).toEqual('green');
        expect(wrapper.vm.randomColor(7)).toEqual('blue');
        expect(wrapper.vm.randomColor(11)).toEqual('blue');
    });
});