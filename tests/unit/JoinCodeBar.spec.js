import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import JoinCodeBar from "../../src/components/scrum/JoinCodeBar";

describe('components/scum/JoinCodeBar - sanity check', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(JoinCodeBar,{
            propsData: {
                joinCode: ''
            }
        });
    });

    it('Test Wrapper works', () => {
        expect(wrapper.exists()).toBe(true);
    });

    it('There is a JoinCode id (used by the Tour)', () => {
        expect(wrapper.find('#join-code-div').exists()).toBe(true);
    });

    it('There is a a href with a title "Copy join link"', () => {
        expect(wrapper.find('a').exists()).toBe(true);
    });

    it('There is a join code placeholder', () => {
        expect(wrapper.find('#join-code-text').exists()).toBe(true);
    });
});

describe('components/scum/JoinCodeBar - check join code', () => {
    let wrapper = null;
    let expectedJoinCode = '00000000';

    beforeEach(() => {
        wrapper = mountWithPlugins(JoinCodeBar, {
            propsData: {
                joinCode: expectedJoinCode
            }
        });
    });

    it('Check if the initiali joinCode is displayed', () => {
        expect(wrapper.find('#join-code-text').text()).toBe(expectedJoinCode);
    });

    it('Check if the value that is being copied is correct', () => {
        let shareUrl = wrapper.vm.shareUrl();
        expect(shareUrl).toContain('?joinCode=' + expectedJoinCode);
    });

    it('Change the join code and see if it is visible', async () => {
        wrapper.setProps({ joinCode: 'QWERT1YU' });
        await wrapper.vm.$nextTick();  // Wait until the new props have been set
        //expect(wrapper.vm.joinCode).to.eql('QWERT1YU')
        //expect(wrapper.props()).to.eql({ joinCode: 'QWERT1YU' })
        expect(wrapper.find('#join-code-text').text()).toBe('QWERT1YU');
    });
});
