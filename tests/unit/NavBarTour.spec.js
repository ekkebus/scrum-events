import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import { Storage } from "../../src/services/Storage";
import NavBarTour from "../../src/components/scrum/NavBarTour"; //the target under test

describe('components/scrum/NavBarTour - sanity check', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(NavBarTour);
    });

    it('Test Wrapper matches snapshot render', () => {
        /*
         If you wish to change the component, you must explicitly delete that directory 
        and generate new snapshots, which can be done by running:
        npm run test:unit -- -u
        */
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('components/scrum/NavBarTour - methods', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(NavBarTour);
    });

    it('Count tutorial up', () => {
        let tempTutorialCounter = Storage.tutorialCounter;
        wrapper.vm.countTutorialUp();
        expect(Storage.tutorialCounter).toBe(tempTutorialCounter + 1);
    });

    it('Count tutorial down', () => {
        wrapper.vm.countTutorialUp();
        wrapper.vm.countTutorialUp();
        wrapper.vm.countTutorialUp();
        let tempTutorialCounter = Storage.tutorialCounter;
        wrapper.vm.countTutorialDown();
        expect(Storage.tutorialCounter).toBe(tempTutorialCounter - 1);
    });

    it('Restart tutorial when game is finished', () => {
        Storage.game.isFinished = true;
        wrapper.vm.restartTutorial();
        expect(Storage.tutorialCounter).toBe(7);
    });
});
