import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import Snackbar from "../../src/components/scrum/Snackbar";

describe('components/scrum/Snackbar - sanity check', () => {
    let wrapper = null;
    let expectedText = 'Error';

    beforeEach(() => {
        wrapper = mountWithPlugins(Snackbar, {
            propsData: {
                text: expectedText
            }
        });
    });

    it('Test Wrapper matches snapshot render', () => {
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('components/scrum/Snackbar - sanity check (error)', () => {
    let wrapper = null;
    let expectedText = 'Ooops I did it again!';

    beforeEach(() => {
        wrapper = mountWithPlugins(Snackbar, {
            propsData: {
                text: expectedText,
                type: 'error'
            }
        });
    });

    it('Snackbar div has error class', () => {
        expect(wrapper.find('div.snackbar.snackbar-error').exists()).toBe(true);
    });

    it('Snackbar contains correct error text', () => {
        expect(wrapper.find('div.snackbar').text()).toBe(expectedText);
    });
});

describe('components/scrum/Snackbar - sanity check (succes)', () => {
    let wrapper = null;
    let expectedText = 'Awesome, great job!';

    beforeEach(() => {
        wrapper = mountWithPlugins(Snackbar, {
            propsData: {
                text: expectedText,
                type: 'success'
            }
        });
    });

    it('Snackbar div has a succes class', () => {
        expect(wrapper.find('div.snackbar.snackbar-success').exists()).toBe(true);
    });

    it('Snackbar contains text', () => {
        expect(wrapper.find('div.snackbar').text()).toBe(expectedText);
    });
});
