import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import { Storage } from "../../src/services/Storage";
import JoinGameModal from "../../src/components/scrum/modals/JoinGameModal";

describe('components/scrum/modal/JoinGameModal - snapshot check', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(JoinGameModal);
    });

    it('Test Wrapper matches snapshot render', () => {
        /*
         If you wish to change the component, generate new snapshots, 
         which can be done by running:
         npm run test:unit -- -u
        */
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('components/scrum/modal/JoinGameModal - methods', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(JoinGameModal);
    });

    it('JoinGameModal open create game', () => {
        wrapper.vm.openCreateGame();
        expect(Storage.openModal.modalType).toBe("startGame");
    });

    it('JoinGameModal open settings', () => {
        wrapper.vm.settings();
        expect(Storage.openModal.modalType).toBe("optionsModal");
        expect(Storage.openModal.closeToType).toBe("joinGame");
    });

    it('JoinGameModal open stats', () => {
        wrapper.vm.graph();
        expect(Storage.openModal.modalType).toBe("statsModal");
    });
});

