import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import { Methods, Storage } from "../../src/services/Storage";

import ResetGameModal from "../../src/components/scrum/modals/ResetGameModal";


describe('components/scrum/modal/ResetGameModal - snapshot check', () => {
    let wrapper = null;

    let closeCallBack = jest.fn();

    beforeEach(() => {
        wrapper = mountWithPlugins(ResetGameModal, {
            propsData: {
                closeModal: closeCallBack
            }
        });
    });

    it('Test Wrapper matches snapshot render', () => {
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('components/scrum/modal/ResetGameModal - methods', () => {
    let wrapper = null;

    let closeCallBack = jest.fn();

    beforeEach(() => {
        wrapper = mountWithPlugins(ResetGameModal, {
            propsData: {
                closeModal: closeCallBack
            }, methods: {
                //mock/overwrite methods within the component
                resetGame: () => {
                }
            }
        });
    });

    it('Close function is called', async () => {
        wrapper.findAll('base-button-stub').at(1).vm.$emit('click');
        await wrapper.vm.$nextTick();    //wait until the emits have been finished
        expect(closeCallBack.mock.calls.length).toBe(1);
        expect(closeCallBack).toBeCalled();
    });
});
