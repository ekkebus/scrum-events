import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import LoadingScreen from "../../src/components/scrum/LoadingScreen";   //the target under test

describe('components/scrum/LoadingScreen - sanity check', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(LoadingScreen);
    });

    it('Divs that will be animated exist', () => {
        expect(wrapper.find('.spinner-inner div div:nth-child(1) div').exists()).toBe(true);
        expect(wrapper.find('.spinner-inner div div:nth-child(2) div').exists()).toBe(true);
        expect(wrapper.find('.spinner-inner div div:nth-child(3) div').exists()).toBe(true);
        expect(wrapper.find('.spinner-inner div div:nth-child(4) div').exists()).toBe(true);
        //counter check
        expect(wrapper.find('.spinner-inner div div:nth-child(5) div').exists()).toBe(false);
    });

    it('Test Wrapper matches snapshot render', () => {
        /*
         If you wish to change the component, you must explicitly delete that directory 
        and generate new snapshots, which can be done by running:
        npm run test:unit -- -u
        */
        expect(wrapper.element).toMatchSnapshot();
    });
});
