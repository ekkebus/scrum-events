import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import { Storage } from "../../src/services/Storage";
import OptionsModal from "../../src/components/scrum/modals/OptionsModal";

describe('components/scrum/modal/OptionsModal - snapshot check', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(OptionsModal);
    });

    it('Test Wrapper matches snapshot render', () => {
        /*
         If you wish to change the component, you must explicitly delete that directory 
        and generate new snapshots, which can be done by running:
        npm run test:unit -- -u
        */
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('components/scrum/modal/OptionsModal - methods', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(OptionsModal);
    });
  
    it('Switch to darkmode', () => {
        Storage.userData.darkMode = false
        wrapper.vm.darkMode = false
        wrapper.vm.darkModeSwitch()
        expect(Storage.userData.darkMode).toBe(true)
        expect(wrapper.vm.darkMode).toBe(true)
    });

    it('Switch to darkmode and back', () => {
        Storage.userData.darkMode = false
        wrapper.vm.darkMode = false
        wrapper.vm.darkModeSwitch()
        wrapper.vm.darkModeSwitch()
        expect(Storage.userData.darkMode).toBe(false)
        expect(wrapper.vm.darkMode).toBe(false)
    });
});