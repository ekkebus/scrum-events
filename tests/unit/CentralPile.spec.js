import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import { Storage } from "../../src/services/Storage";
import CentralPile from "../../src/components/scrum/CentralPile";

describe('components/scrum/CentralPile - Snapshot check', () => {
    let wrapper = null;

    const testCard = {
        id: 1234,
        description: "Description 1234",
        column: null,
        explanation: "Blabla",
        note: null,
        correctColumnId: 7
    }

    beforeEach(() => {
        wrapper = mountWithPlugins(CentralPile, {
            propsData: {
                currentCard: testCard
            }
        });
    });

    it('Test Wrapper matches snapshot render', () => {
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('components/scrum/CentralPile - functions', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(CentralPile, {
            propsData: { currentCard: {} }
        });
    });

    it('show sidebar', () => {
        wrapper.vm.hideSideBar();
        expect(Storage.showSideBar).toBe(false);
    });
});