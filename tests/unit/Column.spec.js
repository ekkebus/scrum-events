import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import Column from "../../src/components/scrum/Column";
import Card from "../../src/components/scrum/Card";

describe('components/scrum/Column - sanity check', () => {
    let expectedColumnId = 7;
    let expectedColumnTitle = "Awesome title";
    let expectedCardId = 77;
    const wrapper = mountWithPlugins(Column, {
        propsData: {
            column: {
                id: expectedColumnId,
                title: expectedColumnTitle,
                cards: [
                    {
                        id: expectedCardId,
                        description: "Card description number uno.",
                        column: null,
                        explanation: "Blabla",
                        note: null,
                        correctColumnId: 0
                    },
                    {
                        id: expectedCardId + 1,
                        description: "Card description number dos",
                        column: null,
                        explanation: "Blabla",
                        note: null,
                        correctColumnId: 0
                    }
                ]
            }
        }
    });

    it('Column has a card in it', () => {
        expect(wrapper.find(Card).exists()).toBe(true);
    });

    it('Column has two cards in it', () => {
        expect(wrapper.findAll(Card).length).toBe(2);
    });

    it('Test Wrapper matches snapshot render', () => {
        expect(wrapper.element).toMatchSnapshot();
    });
});