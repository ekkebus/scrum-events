import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import { Storage } from "../../src/services/Storage";
import Timer from "../../src/components/scrum/Timer";

describe('components/scrum/Timer - sanity check', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(Timer);
    });

    it('Default time is 00:00 (with double zero\'s)', () => {
        expect(wrapper.vm.minutes).toBe('00');
        expect(wrapper.vm.seconds).toBe('00');
    });

    it('Default time is displayed (minutes and seconds)', () => {
        expect(wrapper.find('span.minute').text()).toBe('00');
        expect(wrapper.find('span.seconds').text()).toBe('00');
    });

    it('Test Wrapper matches snapshot render', () => {
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('components/scrum/Timer - methods', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(Timer);
    });

    it('show timer popout', () => {
        wrapper.vm.showTimerDropdown();
        expect(wrapper.vm.timerPopoutShown).toBe(true);
    });

    it('Returned time is 10:00 (positive)', () => {
        Storage.timer.currentTime = 600;
        expect(wrapper.vm.minutes).toBe('10');
        expect(wrapper.vm.seconds).toBe('00');
    });

    it('Returned time is 09:59 (positive)', () => {
        Storage.timer.currentTime = 599;
        expect(wrapper.vm.minutes).toBe('09');
        expect(wrapper.vm.seconds).toBe('59');
    });

    it('Returned time is -10:00 (negative)', () => {
        Storage.timer.currentTime = -600;
        expect(wrapper.vm.minutes).toBe('-10');
        expect(wrapper.vm.seconds).toBe('00');
    });

    it('Returned time is formatted correctly (below 60 seconds)', () => {
        Storage.timer.currentTime = 0;
        expect(wrapper.vm.minutes).toBe('00');
        expect(wrapper.vm.seconds).toBe('00');

        Storage.timer.currentTime = 1;
        expect(wrapper.vm.minutes).toBe('00');
        expect(wrapper.vm.seconds).toBe('01');

        Storage.timer.currentTime = 7;
        expect(wrapper.vm.minutes).toBe('00');
        expect(wrapper.vm.seconds).toBe('07');

        Storage.timer.currentTime = 9;
        expect(wrapper.vm.minutes).toBe('00');
        expect(wrapper.vm.seconds).toBe('09');

        Storage.timer.currentTime = 10;
        expect(wrapper.vm.minutes).toBe('00');
        expect(wrapper.vm.seconds).toBe('10');

        Storage.timer.currentTime = 11;
        expect(wrapper.vm.minutes).toBe('00');
        expect(wrapper.vm.seconds).toBe('11');
        
        Storage.timer.currentTime = 59;
        expect(wrapper.vm.minutes).toBe('00');
        expect(wrapper.vm.seconds).toBe('59');
    });

    it('Returned time is formatted correctly (negative below 60 seconds)', () => {
        Storage.timer.currentTime = -0;
        expect(wrapper.vm.minutes).toBe('00');
        expect(wrapper.vm.seconds).toBe('00');

        Storage.timer.currentTime = -1;
        expect(wrapper.vm.minutes).toBe('-00');
        expect(wrapper.vm.seconds).toBe('01');

        Storage.timer.currentTime = -9;
        expect(wrapper.vm.minutes).toBe('-00');
        expect(wrapper.vm.seconds).toBe('09');
    });

    //TODO addToTime
});
