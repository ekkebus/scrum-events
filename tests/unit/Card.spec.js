import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import Card from "../../src/components/scrum/Card";

describe('components/scrum/Card - Sanity check', () => {
  let wrapper = null;
  let expectedCardId = 123;
  let exepctedCardDescription = "This is an awesome description";

  beforeEach(() => {
    wrapper = mountWithPlugins(Card, {
      propsData: {
        card: {
          id: expectedCardId,
          description: exepctedCardDescription,
          column: null,
          explanation: "Blabla",
          note: null,
          correctColumnId: 0
        },
        isAbsolute: false
      }
    });
  });

  it('Card the correct description in the body', () => {
    expect(wrapper.find('div.sc-card-body span').text()).toBe(exepctedCardDescription);
  });

  it('Test Wrapper matches snapshot render', () => {
    expect(wrapper.element).toMatchSnapshot();
  });
});

describe('components/scrum/Card - Click check', () => {
  let wrapper = null;
  let expectedCardId = 456;

  const mockMethods = {
    showNoteModal: jest.fn(),
    showExplanation: jest.fn()
  };
  beforeEach(() => {
    wrapper = mountWithPlugins(Card, {
      propsData: {
        card: {
          id: expectedCardId,
          description: "This is an awesome description 2",
          column: null,
          explanation: "Blabla",
          note: null,
          correctColumnId: 0
        },
        isAbsolute: false
      },
      mockMethods
    });
  });

  /*
  //TODO
  xit('Click on note opens the NoteModal', async () => {
    expect(wrapper.find('div#noteAlert456').exists()).toBe(true)
    await wrapper.find('div#noteAlert456').trigger('click')
    expect(mockMethods.showNoteModal).toHaveBeenCalled()
  })
  */
});