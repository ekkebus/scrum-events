import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import RegisterModal from "../../src/components/scrum/modals/RegisterModal";

describe('components/scum/modals/RegiserModal - snapshot check', () => {
    const wrapper = mountWithPlugins(RegisterModal);

    it('Test Wrapper matches snapshot render', () => {
        /*
         If you wish to change the component, generate new snapshots, 
         which can be done by running:
         npm run test:unit -- -u
        */
        expect(wrapper.element).toMatchSnapshot();
    });

    it('There is an active button', () => {
        expect(wrapper.find('base-button-stub').exists()).toBe(true)
        expect(wrapper.find('base-button-stub').text()).toBe('Continue...')
        //wrapper.find('base-button-stub').trigger('click')
        //expect().toBe(true)
    });
});

describe('components/scum/modals/RegisterModal - metods', () => {
    const wrapper = mountWithPlugins(RegisterModal, {
        propsData: {
            userName: ""
        }
    });

    it('RegisterModal.testUserName() - Correct user names', () => {
        expect(wrapper.vm.testUserName("JOHN")).toBe(true);
        expect(wrapper.vm.testUserName("john")).toBe(true);
        expect(wrapper.vm.testUserName("Swen")).toBe(true);
        expect(wrapper.vm.testUserName("Swen-Peter")).toBe(true);
        expect(wrapper.vm.testUserName("JOHN_DOE")).toBe(true);
        expect(wrapper.vm.testUserName("j0Hn_-d03")).toBe(true);
        expect(wrapper.vm.testUserName("--")).toBe(true);
        expect(wrapper.vm.testUserName("__")).toBe(true);
        expect(wrapper.vm.testUserName("Wolfeschlegelteinhausenergerdorffelcheorlternarenewissenhaftchafersessenchafearenohlepflegendorgfaltigkeiteschutzenorngreifenurchhraubgierigeindeelcheorlternwolfhundertausendahresoranierscheinenonerrsterdemenscheraumschiffenachtitungsteinndiebenridiumlektrischotorsebrauchichtlseinrsprungonraftestarteinangeahrtinzwischenternartigaumuferuchenachbarschafterternelcheehabtewohnbarlanetenreiserehenichndohinereueasseonerstandigenschlichkeitonnteortpflanzenndichrfreuennebenslanglichreudenduheitichtinurchtorngreifenorndererntelligenteschopfsoninzwischenternartigaum")).toBe(true);
    });

    it('RegiserModal.testUserName() - Incorrect user names, too small', () => {
        expect(wrapper.vm.testUserName("")).toBe(false);
        expect(wrapper.vm.testUserName("J")).toBe(false);
        expect(wrapper.vm.testUserName("j")).toBe(false);
        expect(wrapper.vm.testUserName("0")).toBe(false);
        expect(wrapper.vm.testUserName("ó")).toBe(false);
        expect(wrapper.vm.testUserName("-")).toBe(false);
        expect(wrapper.vm.testUserName("_")).toBe(false);
        expect(wrapper.vm.testUserName("?")).toBe(false);
        expect(wrapper.vm.testUserName(">")).toBe(false);
    });

    it('RegiserModal.testUserName() - Incorrect user names', () => {
        expect(wrapper.vm.testUserName(" JOHN")).toBe(false);
        expect(wrapper.vm.testUserName("john ")).toBe(false);
        expect(wrapper.vm.testUserName("JO HN")).toBe(false);
        expect(wrapper.vm.testUserName("JO HN ")).toBe(false);
        expect(wrapper.vm.testUserName(" JO HN ")).toBe(false);
        expect(wrapper.vm.testUserName("Swen!")).toBe(false);
        expect(wrapper.vm.testUserName("Swen-Peter Ekkebus")).toBe(false);
        expect(wrapper.vm.testUserName("JOHN-DOE💩")).toBe(false);
        expect(wrapper.vm.testUserName("j0Hn_-d03^")).toBe(false);

        expect(wrapper.vm.testUserName("jóhn")).toBe(false);
        expect(wrapper.vm.testUserName("john!")).toBe(false);
        expect(wrapper.vm.testUserName("j@hn")).toBe(false);
    });
});
