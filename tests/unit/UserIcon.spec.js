import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import UserIcon from "../../src/components/scrum/UserIcon";

describe('components/scrum/UserIcon - sanity check', () => {
    let wrapper = null;
    let player = {
        userName: "Dude"
    };

    beforeEach(() => {
        wrapper = mountWithPlugins(UserIcon, {
            propsData: {
                text: player.userName,
                color: 'red',
                active: true,
                isMe: true
            }
        });
    });

    it('There is a span with the UserName', () => {
        expect(wrapper.find('div.sc-user-icon span').exists()).toBe(true);
        expect(wrapper.find('div.sc-user-icon span').text()).toBe(player.userName);
    });

    it('The UserIcon has the correct background color', () => {
        expect(wrapper.classes()).toContain('bg-red');
        expect(wrapper.classes()).not.toContain('bg-purple');
        expect(wrapper.classes()).not.toContain('bg-green');
        expect(wrapper.classes()).not.toContain('bg-blue');
    });

    it('The UserIcon has the "turn" class', () => {
        expect(wrapper.classes()).toContain('sc-user-icon-active');
    });

    it('The UserIcon has the "isMe" class', () => {
        expect(wrapper.classes()).toContain('sc-user-icon-isme');
    });

    it('Test Wrapper matches snapshot render', () => {
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('components/scrum/UserIcon - property check', () => {
    let wrapper = null;
    let player = {
        userName: "Dude"
    };

    beforeEach(() => {
        wrapper = mountWithPlugins(UserIcon, {
            propsData: {
                text: player.userName,
                color: 'blue',
                active: false,
                isMe: false
            }
        });
    });

    it('The UserIcon has the correct background color', () => {
        expect(wrapper.classes()).toContain('bg-blue');
        expect(wrapper.classes()).not.toContain('bg-purple');
        expect(wrapper.classes()).not.toContain('bg-green');
        expect(wrapper.classes()).not.toContain('bg-red');
    });

    it('The UserIcon has the "turn" class (false)', () => {
        expect(wrapper.classes()).not.toContain('sc-user-icon-active');
    });

    it('The UserIcon has the "isMe" class (false)', () => {
        expect(wrapper.classes()).not.toContain('sc-user-icon-isme');
    });
});