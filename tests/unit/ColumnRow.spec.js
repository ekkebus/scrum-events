import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import ColumnRow from "../../src/components/scrum/ColumnRow";

describe('components/scrum/ColumnRow - sanity check', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(ColumnRow, {
            propsData: {
                columns: [{
                    id: 0,
                    title: "Column title",
                    cards: [
                        {
                            id: 0,
                            description: "Desccc",
                            column: null,
                            explanation: "Blabla",
                            note: null,
                            correctColumnId: 0
                        }
                    ]
                }]
            }
        });
    });

    it('Test Wrapper matches snapshot render', () => {
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('components/scrum/ColumnRow - methods', () => {
    const wrapper = mountWithPlugins(ColumnRow, {
        propsData: {
            columns: [{
                id: 0,
                title: "Column title",
                cards: [
                    {
                        id: 0,
                        description: "Desccc",
                        column: null,
                        explanation: "Blabla",
                        note: null,
                        correctColumnId: 0
                    }
                ]
            }]
        }
    });

    it('Show copyright string', () => {
        wrapper.vm.openCloseCopyrights();
        expect(wrapper.vm.isVisible).toBe(true);
    });

    it('Hide copyright string', () => {
        wrapper.vm.openCloseCopyrights();
        expect(wrapper.vm.isVisible).toBe(false);
    });

    it('Show copyright string again', () => {
        wrapper.vm.openCloseCopyrights();
        expect(wrapper.vm.isVisible).toBe(true);
    });
});