import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import { Storage } from "../../src/services/Storage";
import SaveNoteModal from "../../src/components/scrum/modals/SaveNoteModal";

describe('components/scrum/modal/SaveNoteModal - snapshot check', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(SaveNoteModal);
    });

    it('Has a header', () => {
        expect(wrapper.find('h6').text()).toBe('Add note');
    });

    it('Has a textarea', () => {
        expect(wrapper.find('textarea').exists()).toBe(true);
    });

    it('Test Wrapper matches snapshot render', () => {
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('components/scrum/modal/SaveNoteModal - methods', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(SaveNoteModal);
    });
  
    it('Update note is saved in storage', () => {
        wrapper.vm.updateNote("TestingNoteModal");
        expect(Storage.openModal.cardNote).toBe("TestingNoteModal");
    });
});