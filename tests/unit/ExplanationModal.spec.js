import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import ExplanationModal from "../../src/components/scrum/modals/ExplanationModal";

describe('components/scrum/modal/ExplanationModal - sanity check', () => {
    let wrapper = null;

    //test data
    let expectedCardDescription = 'The Expected card descirption';
    let expectedCorrectColumTitle = 'The correct one';
    let expectedActualColumTitle = 'The actual one';

    let testCard = {
        id: 5,
        description: expectedCardDescription,
        column: null,
        explanation: "Blabla",
        note: null,
        correctColumnId: 0
    }
    let closeCallBack = jest.fn();

    beforeEach(() => {
        wrapper = mountWithPlugins(ExplanationModal, {
            propsData: {
                cardData: testCard,
                closeModal: closeCallBack
            }, methods: {
                //mock/overwrite methods within the component
                correctColumnTitle: (card) => {
                    return expectedCorrectColumTitle
                },
                actualColumnTitle: (card) => {
                    return expectedActualColumTitle
                }
            }
        });
    });

    it('There is a header with the correct description', () => {
        expect(wrapper.find('h6').exists()).toBe(true);
        expect(wrapper.find('h6').text()).toBe(expectedCardDescription);
    });

    it('There is a section with the actual / given answer', () => {
        expect(wrapper.find('vue-markdown-stub:nth-child(1)').text()).toBe('Your answer: The actual one');
    });

    it('There is a section with the right answer', () => {
        expect(wrapper.find('vue-markdown-stub:nth-child(2)').text()).toBe('Right answer: The correct one');
    });

    it('There is a section with the card explanation', () => {
        expect(wrapper.find('vue-markdown-stub:nth-child(3)').text()).toBe(testCard.explanation);
    });

    it('Test Wrapper matches snapshot render', () => {
        expect(wrapper.element).toMatchSnapshot();
    });

    //it('Close function is called', async () => {
        //TODO needs fixing
        //wrapper.vm.$emit('close')
        //await wrapper.vm.$nextTick();    //wait until the emits have been finished
        //expect(closeCallBack.mock.calls.length).toBe(1)
    //});
});
