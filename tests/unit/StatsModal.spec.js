import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import { Storage } from "../../src/services/Storage";
import StatsModal from "../../src/components/scrum/modals/StatsModal";

describe('components/scrum/modal/StatsModal - sanity check', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(StatsModal, {
            methods: {
                mounted() {
                    /*fixes async issue in tests*/
                    console.log('*****************************')
                }
            }
        });
    });

    it('Test Wrapper matches snapshot render', () => {
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('components/scrum/modal/StatsModal - methods', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(StatsModal);
    });

    it('StatsModal exit stats', () => {
        wrapper.vm.exit();
        expect(Storage.openModal.modalType).toBe("joinGame");
    });

    it('CalculateHours returns number of hours', () => {
        var start = new Date();
        start.setHours(start.getHours()-2); //fix timezone (server is GMT)

        expect(wrapper.vm.calculateHours(start)).toBe(0);

        start.setHours(start.getHours()+1);
        expect(wrapper.vm.calculateHours(start)).toBe(1);
    });
});