import { randomString } from "../../../src/utils/stringUtils"

describe('Utils/stringUtils', () => {

    it("randomString function existst", () => {
        expect(typeof randomString).toBe("function")
    })

    it("returns a random string with a given length (default)", () => {
        expect(randomString().length).toBe(7)  //default
    })

    it("returns a random string with a given length (2)", () => {
        expect(randomString(2).length).toBe(2)
    })
})