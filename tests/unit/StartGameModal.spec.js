import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import { Storage } from "../../src/services/Storage";
import StartGameModal from "../../src/components/scrum/modals/StartGameModal";

describe('components/scrum/modal/StartGameModal - snapshot check', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(StartGameModal);
    });

    it('Test Wrapper matches snapshot render', () => {
        expect(wrapper.element).toMatchSnapshot();
    });
})

describe('components/scrum/modal/StartGameModal - methods', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(StartGameModal);
    })

    it('StartGameModal switch value', () => {
        wrapper.vm.dirFeedback = false;
        wrapper.vm.valueSwitch();
        expect(wrapper.vm.dirFeedback).toBe(true);
        wrapper.vm.valueSwitch();
        expect(wrapper.vm.dirFeedback).toBe(false);
    });

    it('StartGameModal open join game', () => {
        wrapper.vm.openJoinGame();
        expect(Storage.openModal.modalType).toBe("joinGame");
    });

    it('StartGameModal open settings', () => {
        wrapper.vm.settings();
        expect(Storage.openModal.modalType).toBe("optionsModal");
        expect(Storage.openModal.closeToType).toBe("startGame");
    });
});