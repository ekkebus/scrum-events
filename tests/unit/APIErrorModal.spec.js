import { mountWithPlugins } from "../test-utils/mountWithPlugins";
import { Storage } from "../../src/services/Storage";
import APIErrorModal from "../../src/components/scrum/modals/APIErrorModal"

describe('components/scrum/modal/APIErrorModal - Snapshot check', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(APIErrorModal);
    });

    it('Test Wrapper matches snapshot render', () => {
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('components/scrum/modal/APIErrorModal - methods', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = mountWithPlugins(APIErrorModal);
    });

    it('APIErrorModal return modal', () => {
        Storage.openModal.originalModalType = "restartGame";
        wrapper.vm.returnModal();
        expect(Storage.openModal.modalType).toBe("restartGame");
    });
});