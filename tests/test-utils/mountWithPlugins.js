import { createLocalVue, shallowMount } from "@vue/test-utils";
import { API, Methods, Storage } from "../../src/services/Storage";
import VueClipboard from "vue-clipboard2";
import VueRouter from "vue-router";
import Argon from "../../src/plugins/argon-kit";
import BootstrapVue from "bootstrap-vue";
import { HubManager } from "../../src/services/HubService";
import { Utils } from "../../src/utils/Utils";
import { Firebase } from "firebase/app";

jest.mock("firebase/app", () => ({
    //this.$root.Firebase.logEvent('placed card in column', this.$root.APIStorage.userData);
    logEvent(event, obj) {
        console.log("MOCKED: this.$root.Firebase.logEvent('placed card in column', this.$root.APIStorage.userData);")
    }
}));

// jest.mock("../../src/services/EventService", () => ({
//     showEventList: jest.fn(() => {
//         return Promise.resolve({ data: 3 })
//     }),
// }));

/** 
  *  This method does a mount with a all the required stuff for non dumb components like VueRouter, Bootstreap etc...
  */
export function mountWithPlugins(componentToMount, options = {}) {
    const localVue = createLocalVue();
    localVue.use(VueRouter);
    localVue.use(Argon);
    localVue.use(BootstrapVue);
    localVue.use(VueClipboard);
    const router = new VueRouter();

    //create the $root
    const $root = {
        data() {
            return {
                HubManager: HubManager,
                APIStorage: Storage,
                APICaller: API,
                APIMethods: Methods,
                Utils: Utils,
                Firebase: Firebase
            }
        }
    }

    // merge the old state with the new state, and replace it in the store
    //const store = createStore();
    //store.replaceState({ ...store.state, ...mockStoreState });

    // mount the component with all the options we added
    return shallowMount(componentToMount, {
        localVue,
        router,
        parentComponent: $root,
        ...options,
    });
}